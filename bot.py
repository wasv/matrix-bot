''' Example Matrix Bot '''

from dataclasses import dataclass
from typing import Optional

import os
import sys
import asyncio
import json
import nio


@dataclass(frozen=True)
class CustomClientConfig(nio.AsyncClientConfig):
    '''Optional Client Configuration Information.'''
    device_id: Optional[str] = ''
    store_path: Optional[str] = 'store'
    creds_path: Optional[str] = 'auth.json'
    ssl: Optional[bool] = None
    proxy: Optional[bool] = None


class CustomClient(nio.AsyncClient):
    ''' Custom Client for an Example Matrix Bot '''
    config = CustomClientConfig()

    def __init__(self, homeserver: str, user: str,
                 config: Optional[CustomClientConfig] = None):

        if config:
            self.config = config

        # Calling super.__init__ means we're running the __init__ method
        # defined in AsyncClient, which this class derives from. That does a
        # bunch of setup for us automatically
        super().__init__(
            homeserver,
            user=user,
            device_id=self.config.device_id,
            store_path=self.config.store_path,
            config=self.config,
            ssl=self.config.ssl,
            proxy=self.config.proxy)

        # if the store location doesn't exist, we'll make it
        if self.config.store_path \
                and not os.path.isdir(self.config.store_path):
            os.mkdir(self.config.store_path)

        # auto-join room invites
        self.add_event_callback(self.cb_autojoin_room, nio.InviteEvent)

        # print all the messages we receive
        self.add_event_callback(self.cb_print_messages, nio.RoomMessageText)

    async def cb_autojoin_room(self, room: nio.MatrixRoom, event: nio.InviteEvent):
        """Callback to automatically joins a Matrix room on invite.

        Arguments:
            room {MatrixRoom} -- Provided by nio
            event {InviteEvent} -- Provided by nio
        """
        await self.join(room.room_id)
        print(f"Joined Room: {room.name} - Is encrypted? {room.encrypted}")

    async def cb_print_messages(self, room: nio.MatrixRoom,
                                event: nio.RoomMessageText):
        """Callback to print all received messages to stdout.

        Arguments:
            room {MatrixRoom} -- Provided by nio
            event {RoomMessageText} -- Provided by nio
        """

        if event.decrypted:
            encrypted_symbol = "🛡 "
        else:
            encrypted_symbol = "⚠️ "
        print(f"{room.display_name} |{encrypted_symbol}| "
              f"{room.user_name(event.sender)}: {event.body}")

    async def login(self, password: Optional[str] = None,
                    device_name: Optional[str] = "",
                    token: Optional[str] = None):
        if os.path.exists(self.config.creds_path):
            with open(self.config.creds_path, "r") as cred_file:
                creds = json.load(cred_file)

            self.restore_login(
                creds['user_id'],
                creds['device_id'],
                creds['access_token'])
        else:
            resp = await super().login(password, device_name, token)
            # check that we logged in succesfully
            if isinstance(resp, nio.LoginResponse):
                creds = {}
                creds['access_token'] = resp.access_token
                creds['user_id'] = resp.user_id
                creds['device_id'] = resp.device_id

                with open(self.config.creds_path, "w") as cred_file:
                    # write the login details to disk
                    json.dump(creds, cred_file)
            else:
                print(f"Failed to log in: {resp}")
                sys.exit(1)

        print("Logged in.")


async def main() -> None:
    ''' Creates and runs Example Matrix Bot Client '''
    config_path = "config.json"
    if 'NIO_CONFIG_PATH' in os.environ:
        config_path = os.environ['NIO_CONFIG_PATH']

    with open(config_path, "r") as cred_file:
        server_config = json.load(cred_file)

    password = None
    if 'NIO_PASSWORD' in os.environ:
        password = os.environ['NIO_PASSWORD']
    elif 'password' in server_config:
        password = config['password']

    config = CustomClientConfig(store_sync_tokens = True)
    client = CustomClient(server_config['home_server'], server_config['user_id'], config)

    await client.login(password)

    device_resp = await client.devices()
    # Removes all old sessions, if password is available.
    if password and len(device_resp.devices) > 1:
        auth = {"type": "m.login.password", "user": client.user_id, "password": password}
        await client.delete_devices([d.id for d in device_resp.devices if d.id != client.device_id], auth)

    try:
        await client.sync_forever(30000, full_state=True)
    finally:
        await client.close()


if __name__ == "__main__":
    try:
        asyncio.run(
            main()
        )
    except KeyboardInterrupt:
        pass
